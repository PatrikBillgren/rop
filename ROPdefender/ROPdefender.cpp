
/*! @file
 *  This is an example of the PIN tool that demonstrates some basic PIN APIs 
 *  and could serve as the starting point for developing your first PIN tool
 */

#include "pin.H"
#include <iostream>
#include <fstream>
#include <stack>

/* ================================================================== */
// Global variables 
/* ================================================================== */
BOOL ROPDetected = false;

std::ostream * out = &cerr;
std::stack<ADDRINT> retstack;

/* ===================================================================== */
// Command line switches
/* ===================================================================== */
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,  "pintool",
    "o", "", "specify file name for MyPinTool output");

KNOB<BOOL>   KnobCount(KNOB_MODE_WRITEONCE,  "pintool",
    "count", "1", "count instructions, basic blocks and threads in the application");


/* ===================================================================== */
// Utilities
/* ===================================================================== */

/*!
 *  Print out help message.
 */
INT32 Usage()
{
    cerr << "This tool prints out the number of dynamically executed " << endl <<
            "instructions, basic blocks and threads in the application." << endl << endl;

    cerr << KNOB_BASE::StringKnobSummary() << endl;

    return -1;
}

/* ===================================================================== */
// Analysis routines
/* ===================================================================== */
VOID push_ret_address(VOID *r_ip) 
{
	ADDRINT *address = (ADDRINT*)r_ip;
	retstack.push(*address);
}
VOID check_ret_address(VOID *r_ip) 
{
	ADDRINT *addrp = (ADDRINT*)r_ip;
	BOOL equal;
	do {
		ADDRINT addr = retstack.top();
		retstack.pop();
		equal = *addrp == addr;
	} while (!equal && !retstack.empty()); // This loop assures that we can handle setjmp/longjmp
	if (!equal){
		ROPDetected = 1;
		cout << "ROP DETECTED!!!" << endl;
	}
}

/* ===================================================================== */
// Instrumentation callbacks
/* ===================================================================== */
VOID instrument_instruction(INS ins, VOID *v) 
{
	if (INS_IsCall(ins)) {
		// Put the top of the stack on the Shadow stack
		INS_InsertCall(ins, IPOINT_TAKEN_BRANCH, (AFUNPTR)push_ret_address, IARG_RETURN_IP, IARG_END);
	} else if (INS_IsRet(ins)) {
		// Compare the TOSSES
		INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)check_ret_address, IARG_BRANCH_TARGET_ADDR, IARG_END);
	}
}

/*!
 * Print out analysis results.
 * This function is called when the application exits.
 * @param[in]   code            exit code of the application
 * @param[in]   v               value specified by the tool in the 
 *                              PIN_AddFiniFunction function call
 */
VOID Fini(INT32 code, VOID *v)
{
    *out <<  "===============================================" << endl;
    *out <<  "ROPdefender analysis results: " << endl;
    *out <<  "ROP detected: " << ROPDetected  << endl;
    *out <<  "===============================================" << endl;
}

/*!
 * The main procedure of the tool.
 * This function is called when the application image is loaded but not yet started.
 * @param[in]   argc            total number of elements in the argv array
 * @param[in]   argv            array of command line arguments, 
 *                              including pin -t <toolname> -- ...
 */
int main(int argc, char *argv[])
{
    // Initialize PIN library. Print help message if -h(elp) is specified
    // in the command line or the command line is invalid 
    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }
    


    if (KnobCount)
    {
        // Register function to be called to instrument instructions
        INS_AddInstrumentFunction(instrument_instruction, 0);

        // Register function to be called when the application exits
        PIN_AddFiniFunction(Fini, 0);
    }
    
    cerr <<  "===============================================" << endl;
    cerr <<  "This application is instrumented by ROPdefender" << endl;
    cerr <<  "===============================================" << endl;

    // Start the program, never returns
    PIN_StartProgram();
    
    return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
