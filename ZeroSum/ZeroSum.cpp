//===- ZeroSum.cpp -                                      ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "zerosum"
#define STRING_LENGTH 25
#include <iostream>
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/ValueSymbolTable.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/ADT/ilist.h"
#include <string>
#include <iterator>
#include <stdlib.h>
#include <time.h>


using namespace llvm;
using std::string;

namespace {
  struct ZeroSum : public FunctionPass {
        string counterRef;
        string errorMessage;

    void createStringRef() {
        srand(time(NULL));
      static const char alphanum[] =
      "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      string name;
      for (int i = 0; i < STRING_LENGTH; ++i) {
        name += alphanum[rand() % (sizeof(alphanum) - 1)];
      }

      counterRef = string(name);
    }

    static char ID; // Pass identification, replacement for typeid
    ZeroSum() : FunctionPass(ID) {}


    /** 
    * Runs one time before all function passes.
    * Creates a global variable initialized to zero with a random name.
    * It generates a new name if the name is already taken.
    */
    virtual bool doInitialization(Module &M) {
      createStringRef();
      while (M.getNamedGlobal(counterRef) != NULL) {
        createStringRef();
      }
      IntegerType *intType = IntegerType::get(M.getContext(), 32);
      ConstantInt * zero = ConstantInt::get(intType, 0);
      // TODO: Needs to be set threadlocal
      new GlobalVariable(M, intType, false, GlobalVariable::ExternalLinkage, zero, counterRef, NULL, GlobalVariable::LocalExecTLSModel);
      IRBuilder<> builder(M.getContext());
      errorMessage = string("ERROR_MESSAGE");
      string msg("ROP-attack detected. Terminating program.\n");

    Constant *strConstant = ConstantDataArray::getString(M.getContext(), msg);
    GlobalVariable *globalErrorMsg = new GlobalVariable(M, strConstant->getType(), true, GlobalValue::PrivateLinkage, strConstant);
    globalErrorMsg->setName(errorMessage);

      return true;
    }

    /**
    * Runs on every function in the program.
    * It creates a prologue, an epilogue and two new basic blocks in the end of the function.
    * The prologue loads the global ZSD-counter, increments it and stores it.
    * The epilogue loads the same counter, decrements it, stores it and compares it with zero.
    * If the counter is less than zero, a ROP-attack is detected and the exit()-function is called.
    **/
    virtual bool runOnFunction(Function &F) {
      errs() << "NOW STARTING FUNCTION " << F.getName().str() << "\n";
      Module *M = F.getParent();
      LLVMContext& context = F.getContext();
      GlobalVariable * counter = M->getNamedGlobal(counterRef);
      GlobalVariable * errorMsg = M->getNamedGlobal(errorMessage);
      // Create an incrementing instruction
      // Insert incrementing instruction

        // Save the last instruction (return instruction) for later use
      Instruction * lastInstr = &*--inst_end(F);

      // Create the IRBuilder with the instruction that the created instructions will supercede
      IRBuilder<> builder(&*inst_begin(F));

        // Prologue
      ConstantInt *one = builder.getInt32(1);
      Value *load = builder.CreateLoad(counter);
      Value* add = builder.CreateAdd(load, one);
      builder.CreateStore(add, counter);

        // New builder at the end of the function
      IRBuilder<> builder2(&*--inst_end(F));

        // Epilogue
      load = builder2.CreateLoad(counter);
      Value* sub = builder2.CreateSub(load, one);
      builder2.CreateStore(sub, counter);
      ConstantInt * zero = builder2.getInt32(0);
      Value * cmp = builder2.CreateICmpSLT(sub, zero);

        // Create the exit block.
      BasicBlock *exitBlock = BasicBlock::Create(context, "Exit-block");
      IRBuilder<> exitBuilder(exitBlock);
      ArrayRef<Type *> params(Type::getInt32Ty(context));
      FunctionType *ftype = FunctionType::get(Type::getVoidTy(context), params, false);
      FunctionType *printfType = FunctionType::get(Type::getInt32Ty(context), true);
      Constant* exitFunction = F.getParent()->getOrInsertFunction("exit", ftype);
      Constant* printf = F.getParent()->getOrInsertFunction("printf", printfType);
      ConstantInt* exitArg = exitBuilder.getInt32(1);
      exitBuilder.CreateCall(printf, errorMsg);
      exitBuilder.CreateCall(exitFunction, exitArg);
      exitBuilder.CreateUnreachable();
      F.getBasicBlockList().push_back(exitBlock);


        //Create the return block, and copy the original return-instruction here
      BasicBlock *returnBlock = BasicBlock::Create(context, "Ret-block");
      Instruction * retInst = lastInstr->clone();
      returnBlock->getInstList().push_back(retInst);
      F.getBasicBlockList().push_back(returnBlock);

      // Create a branch in the end of the original basic block, and remove the old return
      Instruction *branch = builder2.CreateCondBr(cmp, exitBlock, returnBlock); 
      lastInstr->replaceAllUsesWith(branch);
      lastInstr->eraseFromParent();
      return true;
    }
  };
}

char ZeroSum::ID = 0;
static RegisterPass<ZeroSum> X("zerosum", "Zero Sum Defender - Detects all ROP-attacks");

