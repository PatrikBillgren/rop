#ifndef GADGET_MAP__H
#define GADGET_MAP__H

#include <iostream>
#include <map>
#include <fstream>
#include "definitions.h"

using namespace std;

class GadgetMap {
    map<address_t, char> addressMap;
    public:
    GadgetMap(string inputfile);
    bool contains (address_t gadget);
    bool containsAll(GadgetMap *gm);
};

struct GadgetFileNotFound {
};


#endif 