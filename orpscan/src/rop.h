#ifndef ROP__H
#define ROP__H


bool isRopAttack(GadgetMap &gm, AddressIterator &itr, ofstream debugstream);
void run(string gadgetFile, string dataFile);
void analyze(GadgetMap &gm, AddressIterator &itr, string filename);
void analyze();

#endif