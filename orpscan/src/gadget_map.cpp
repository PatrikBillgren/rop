#include "gadget_map.h"
#include <string>
using namespace std;



GadgetMap::GadgetMap(string inputfile) {
    ifstream ifile(inputfile, ios::out);
    if (!ifile) {
        cout << inputfile << " not found!\n";
           throw new GadgetFileNotFound;
    }
    int numberOfGadgets;
    ifile >> numberOfGadgets;

    // We read all the gadgets and save them in a map
    for (int i = 0; i < numberOfGadgets; ++i) {
        address_t addr;
        ifile >> addr;
        addressMap.insert({addr, 1});
        //cout << "Adding " << hex << addr << endl;
    }
    ifile.close();
    cout << "Database contains " << dec << addressMap.size() << " gadgets." << endl;
}

bool 
GadgetMap::contains (address_t gadget) {
    auto itr = addressMap.find(gadget);
    return itr != addressMap.end();
}

bool
GadgetMap::containsAll(GadgetMap *gm) {
	if (addressMap.size() < gm->addressMap.size())
		return false;
	for (auto itr = gm->addressMap.cbegin(); itr != gm->addressMap.cend(); ++itr)
		if (!this->contains((*itr).first)) {
			return false;
		}
	return true;
}


