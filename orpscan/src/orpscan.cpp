#include "orpscan/orpscan/stdafx.h"
#include "boost/program_options.hpp"
#include "rop.h"
#include "definitions.h"

using namespace std;

unsigned int gadgets = 1000;
unsigned int maxspaceLimit = 100;

string ORP_PATH = "C:\\Users\\Patrik\\Documents\\ROP\\orp-0.3";
const string GADGET_SUFFIX = ".gadgets";

void runTest()
{
    run("../../../data/gadgets.in", "../../../data/data.in");
}

void randomize(string binaryPath)
{
	system(string(string("python ") + ORP_PATH + "\\orp.py -i " + binaryPath).c_str());
}
void test(string binaryPath, string dataPath)
{
	run(string(binaryPath + GADGET_SUFFIX), dataPath);
}

void randomizeTest(string binaryPath, string dataPath)
{
    // 1. Analyze files other than the ones included in the test directory. To do so,
    // you have to extract the control-flow graph:
    // > python orp.py -d path/to/dll/or/exe

    // 2. Coverage evaluation
    // Extracts all the gadgets (instruction sequences that end in an indirect
    // branch; at most five instructions in length) and then applies all the
    // different randomization techniques. Example:
    // > python orp.py -c test\md5\md5.dll
    // After it finishes, a detailed report is shown.


    // Program options using Boost
    // Different options.
    // 1. Randomize the binary and create a file which ORPSCAN can read.
    // 2. Start ORPSCAN for a running binary which has a gadget-file. Specify input file
    // 3
    // We need to define two different parameters. 
    // The number of ROP-addresses needed and the maximum distance between them.
	randomize(binaryPath);
	test(binaryPath, dataPath);
}


namespace po = boost::program_options;
int main (int argc, char **argv) 
{

    // First we want to create the file with all gadgets
	// gadgets.in contains all known gadgets in the program we want to protect
	po::options_description desc("Allowed options");
	

	string binary_path, data_path, orp_path;
	//unsigned int gadgets = 5, maxspace = 5;

	desc.add_options()
		("help", "display help")
		("randomize", "randomize and create new binary with gadget-file for distribution (req. binary-path)")
		("test", "test data file with already randomized binary (req. binary-path and data-path)")
		("randomize-test", "randomize, create new and test binary with data file. (req. binary-path and data-path)")
		("TEST", "for testing")
		("binary-file", po::value<string>(&binary_path), "path to binary")
		("data-file", po::value<string>(&data_path), "path to data")
		("orp-path", po::value<string>(&orp_path), "path to orp")
		("gadgets", po::value<unsigned int>(&gadgets), "number of subsequent gadgets")
		("maxspace", po::value<unsigned int>(&maxspaceLimit), "maximum space (bytes) between gadgets")
		;

	po::positional_options_description p;
	p.add("binary-file", 1);
	p.add("data-file", 1);
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
	po::notify(vm);

	
	if (vm.count("help")) {
		cout << desc << endl;
		return 0;
	}
	if (vm.count("orp-path"))
		ORP_PATH = orp_path;

    if (vm.count("TEST"))
            analyze();

	if (!vm.count("binary-file")){ // We require a binary path always
		cout << desc << endl;
		return 0;
	}

	if (vm.count("randomize"))
		randomize(binary_path);

	if (!vm.count("binary-file") || !vm.count("data-file")){
		cout << desc << endl;
		return 0;
	}
	if (vm.count("randomize-test"))
		randomizeTest(binary_path, data_path);
	if (vm.count("test"))
		test(binary_path, data_path);

	//cout << binary_path << " and " << dec << data_path << " " << gadgets << " " << maxspace << endl;
	return 0;
}
