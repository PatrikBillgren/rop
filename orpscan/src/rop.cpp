
#include "gadget_map.h"
#include "address_iterator.h"
#include "limits.h"

extern unsigned int gadgets; // Use the variables from orpscan.cpp
extern unsigned int maxspaceLimit;


using namespace std;
bool isRopAttack(GadgetMap &gm, AddressIterator &itr)
{
	int counter = 0;
	unsigned int gadgetcounter = 0;
	unsigned int spacecounter = 0;
	bool gFound = false;
	while (!itr.eosb){
		if (gm.contains(*itr)){
			gFound = true;
			cout << "A ROP-gadget is found with address " << *itr << ".\n";
			if (spacecounter > maxspaceLimit)
				gadgetcounter = spacecounter = 0;
			++gadgetcounter;
			if (gadgetcounter == gadgets)
				return true;
			spacecounter = 0;
		}
		++itr;
		++counter;
		if (gFound)
			++spacecounter;
	}
	return false;
}

void analyze(GadgetMap &gm, AddressIterator &itr, ofstream& fstream) {
	//ofstream file(filename, ofstream::out | ofstream::app);
	int counter = 0;
	unsigned int gadgetcounter = 0;
	unsigned int spacecounter = 0;
	unsigned long long totalDist = 0;
	unsigned int maxspace = 0;
	unsigned int minspace = UINT_MAX, totgadgets = 0;
	while (!itr.eosb){
		if (gm.contains(*itr)){
			++totgadgets;
			totalDist += spacecounter;
			if (spacecounter != 0) {
				if (spacecounter < minspace)
					minspace = spacecounter;
				if (spacecounter > maxspace)
					maxspace = spacecounter;
			}
			cout << "A ROP-gadget is found with address " << *itr << ".\n";
			if (spacecounter > maxspaceLimit)
				gadgetcounter = spacecounter = 0;
			++gadgetcounter;
			
			spacecounter = 0;
		}
		++itr;
		++counter;
		if (totgadgets > 0)
			++spacecounter;
	}
	double average = static_cast<double>(totalDist) / static_cast<double>(totgadgets);
	fstream << "Looked through " << counter << " addresses.\n";
	fstream << "Found " << dec << totgadgets << "\n gadgets with min space: " << minspace;
	fstream << " Max space : " << maxspace << " Average: " << average << "\n\n";
}

void run(string gadgetFile, string dataFile) 
{

    GadgetMap gm(gadgetFile);
    ifstream datafile(dataFile, ios_base::in | ios_base::binary);
	
    AddressIterator itr(datafile);
	if (isRopAttack(gm, itr))
		cout << "ROP ATTACK, STOP EXECUTING" << endl;
    
	
}

void hanalyze(string g, string d, ofstream&stream) {
	stream << g.c_str() << " " << d.c_str() <<"\n";
	GadgetMap gm("C:\\Users\\Patrik\\Documents\\ROP\\Tests\\" + g);
	ifstream datastream("C:\\Users\\Patrik\\Documents\\TestInput\\" + d, ios_base::in | ios_base::binary);
	AddressIterator itr(datastream);
	analyze(gm, itr, stream);

}

void analyze() {
	//--test --data-file C:\Users\Patrik\Documents\TestInput\payloads\IntelManual.pdf --binary-file C:\Users\Patrik\Documents\ROP\Tests\msvcr71\msvcr71.dll
	
	
	ofstream file("result2.res", ofstream::out | ofstream::app);

	/*hanalyze("msvcr71\\msvcr71.dll.gadgets", "PDF\\IntelManual.pdf", file);
	hanalyze("msvcr71\\msvcr71.dll.gadgets", "PDF\\GeologyUmnakBogoslof.pdf", file);
	hanalyze("msvcr71\\msvcr71.dll.gadgets", "PDF\\IKEA_Catalog_2014_USA.pdf", file);
	hanalyze("msvcr71\\msvcr71.dll.gadgets", "PDF\\Bible_King_James_Version.pdf", file);
	file.close();
	file.open("result.res", ofstream::out | ofstream::app);
	hanalyze("msvcr71\\msvcr71.dll.gadgets", "random\\test100", file);
	hanalyze("msvcr71\\msvcr71.dll.gadgets", "random\\test1000", file);
	
	hanalyze("hxds\\hxds.dll.gadgets", "PDF\\IntelManual.pdf", file);
	hanalyze("hxds\\hxds.dll.gadgets", "PDF\\GeologyUmnakBogoslof.pdf", file);
	hanalyze("hxds\\hxds.dll.gadgets", "PDF\\IKEA_Catalog_2014_USA.pdf", file);
	hanalyze("hxds\\hxds.dll.gadgets", "PDF\\Bible_King_James_Version.pdf", file);
	file.close();
	file.open("result.res", ofstream::out | ofstream::app);
	hanalyze("hxds\\hxds.dll.gadgets", "random\\test100", file);
	hanalyze("hxds\\hxds.dll.gadgets", "random\\test1000", file);

	hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "PDF\\IntelManual.pdf", file);
	hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "PDF\\GeologyUmnakBogoslof.pdf", file);
	hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "PDF\\IKEA_Catalog_2014_USA.pdf", file);
	hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "PDF\\Bible_King_James_Version.pdf", file);
	file.close();*/
	//file.open("result.res", ofstream::out | ofstream::app);
	//hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "random\\test100", file);
	//hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "random\\test1000", file);

	//hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "payloads\\filter.rop", file);
	//hanalyze("hxds\\hxds.dll.gadgets", "payloads\\hxds.rop", file);
	//hanalyze("msvcr71\\msvcr71.dll.gadgets", "payloads\\msvcr71.rop", file);
	//hanalyze("msvcr71\\msvcr71.dll.gadgets", "payloads\\msvcr712.rop", file);
	hanalyze("Mini-stream\\MSRMfilter03.dll.gadgets", "payloads\\crash.m3u", file);

	file.close();

}
