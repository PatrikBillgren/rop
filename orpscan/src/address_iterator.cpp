#include "address_iterator.h"

using namespace std;


AddressIterator::AddressIterator(ifstream &file):file(&file), iit(file), eosb(false), counter(8){
    for (int i = 0; i < ADDRESS_LENGTH; ++i){
        buffer[i] = *iit;
        //cout << "Buffer " << i << " is " << hex<< static_cast<int>(*iit) << endl;
        ++iit;
    }
}

bool 
AddressIterator::operator!=(const AddressIterator& bsi) const {
        return iit != bsi.iit;
    }
AddressIterator& 
AddressIterator::operator++() {
        ++iit;
        if (file->eof()) {
            eosb = true;
            return *this;
        }
        buffer[counter % ADDRESS_LENGTH] = *iit;
        ++counter;
        return *this;
    }

address_t 
AddressIterator::operator*() {
    if (eosb)
        return 0;
    address_t returnv = 0;
    for (int i = 0; i < ADDRESS_LENGTH; ++i) {
        returnv = returnv << 8;
        //std::cout << "Trying " << (counter + i) % ADDRESS_LENGTH << std::endl;
        returnv += buffer[(counter - i - 1) % ADDRESS_LENGTH];
    }
    return returnv;
}

AddressIterator& 
AddressIterator::operator=(const AddressIterator& adri) {
    iit = adri.iit;
    return *this;
}

