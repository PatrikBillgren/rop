#ifndef ADDRESS_ITERATOR__H
#define ADDRESS_ITERATOR__H

#include <iterator>
#include <iostream>
#include <fstream>
#include "definitions.h"

/**
 * Iterator that let us slide 32 bit over an ifstream, 8 bit each increment
 * */
using namespace std;

class AddressIterator {
	istream_iterator<unsigned char> iit;
	istream_iterator<unsigned char> eos;
	unsigned char buffer[ADDRESS_LENGTH];
	int counter;
	ifstream *file;

public: 
	bool eosb;
	AddressIterator(ifstream &file);
	AddressIterator& operator=(const AddressIterator& adri);
	address_t operator*();
	AddressIterator &operator++();
	bool operator!=(const AddressIterator& bsi) const ;
};



#endif